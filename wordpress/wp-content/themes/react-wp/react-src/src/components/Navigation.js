
import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";

import Home from '../pages/Home';
import About from '../pages/About';

let Navigation = (props)=>{
    
    let navStyle = {
        nav: {
            listStyle:"none",
            display: "flex",
            alignItem: "center",
            justifyContent: 'center',
            width:'100%'
        },

        listStyle: {
            margin: '10px',
            textDecoration:"none"
        }
    }
    

    let createView = (data)=> {
        
        let navView = null;

        navView = props.data.map((obj, i)=>{
            
        return <li key={i} style={{...navStyle.listStyle}}><Link to={obj.slug}>{obj.title.rendered}</Link></li>

        })

        return navView;
    }

    let createHomePage = () => {

    }


    if(props.data !== null){

        return (
            <Router>
                <ul style={{...navStyle.nav}}>
                    {createView(props.data)}         
                </ul>
                <Switch>
                    <Route exact path="/">
                        <Home data={props.data}/>
                    </Route>
                    <Route exact path="/home-page">
                        <Home />
                    </Route>
                    <Route path='/about'>
                        <About />
                    </Route>
            </Switch>
            </Router>
        )
    }

    else {
        return (
            <p>no data </p>
        )
    }

}

export default Navigation;