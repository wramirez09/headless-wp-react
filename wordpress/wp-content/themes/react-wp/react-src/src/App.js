import React, {useState, useEffect} from "react";
import logo from "./logo.svg";
import "./App.css";
import Navigation from "./components/Navigation";
import GetPageData from './services/GetPagesData'

    
const App = () => {

    let [data, setData] = React.useState(null);
    data = GetPageData();

    useEffect(() => {
                
        setData(data)

    }, []);
    
    return (
        <div className="App">
            <Navigation data={data}></Navigation>   
        </div>
    );
}

export default App;
