import React, {useState, useEffect} from "react";
import axios from "axios";

const GetPageData = (props) => {
    
    let [data, setData] = React.useState(null);
    const pagesEndpoint = "http://localhost:8888/index.php/wp-json/wp/v2/pages";

    useEffect(() => {

        const fetchData = async () => {

          try {
            axios.get(pagesEndpoint).then((res)=>{
                
                if(res.status === 200){
                    setData(res.data);  
                }
                
            });
          } catch (e) {
            console.log(e)
          }
        };
    
        fetchData();
        
    }, []);


    return data;

}

export default GetPageData;