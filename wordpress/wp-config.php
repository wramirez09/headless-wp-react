<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'headless-wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'h`gUv0%TD>c[9;+`+fMy8;(ze.gm[zI{QwL2EKw`q#TxjV.:h4Mb>+7g&4/2Ix]{' );
define( 'SECURE_AUTH_KEY',  ':dDM~ EM]@f y2VPAP9N]i#oL1Fa_%DrlYGGnS1jo}5E-o@E]5F?7l^QQg]WZ?pT' );
define( 'LOGGED_IN_KEY',    '~!4N}gHnHt0Rf!NJ?s,0%LNr3*g~RcbI&v60%)x+HP2-Ogsp_U(>D (}.H?U3`kq' );
define( 'NONCE_KEY',        'n;Qt#X>?oq]lXkgj]%W3r.-9O]J$e>9#cIstx0+!UDHe>2U6bJ>{dF9WN|+[ysvH' );
define( 'AUTH_SALT',        'lS<K9mhRtHr<fVu#_bSlRd{pQqRLCh}[&8[p/|+G<x-TmKxjZgd|,LVd~skla:HV' );
define( 'SECURE_AUTH_SALT', ',=XAR%6QeR)97!!jfSwJF*^;vhqG,^4GkOU4+=ft#9~[k#&fDg7s7?x|EtF8Li! ' );
define( 'LOGGED_IN_SALT',   'NHl.:w!V@&]7;eD,kjdP~6jsI?BJe{,]M>&%-ib#fzDS*wI:o #l1LEM#& _}.S%' );
define( 'NONCE_SALT',       'Rc4/Zeo~%P$m@%A=T5K0l@dpdd[RboRl7Szf`Vq[42++S<y-UT[HUsx|Q9%+d^-O' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
